/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio;
import static java.lang.System.console;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class sqlUsuarios extends conexion {
    
    public boolean loginUser(usuarios usr){
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = getConexion();
        String sql = "SELECT users.username,users.password FROM users WHERE users.username=? AND users.password=?";
        try{
            ps=con.prepareStatement(sql);
            ps.setString(1, usr.getUsername());
            ps.setString(2, usr.getPassword());
            rs=ps.executeQuery();
            if(rs.next()){
               return true;
            }
            return false;
        }catch(SQLException ex){
        Logger.getLogger(sqlUsuarios.class.getName()).log(Level.SEVERE, null,ex);
        return false;
        }
    }
    
    
}
