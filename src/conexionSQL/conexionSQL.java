
package conexionSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

//Aca abrimos la conexion 
public class conexionSQL {
      static String bd="laboratorio";
      static String login="admin";
      static String pass="123";
      static String url="jdbc:mysql://localhost/"+bd+"?useTimeZone=true&serverTimezone=UTC";
    Connection conectar=null;
    public Connection conexion(){
  
    
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            conectar=DriverManager.getConnection(url,login,pass);
            JOptionPane.showMessageDialog(null,"Conexion Exitosa");
            
        } catch (Exception e){
          JOptionPane.showMessageDialog(null,"Error al conectar" + e.getMessage()); //En caso de no conectar e. es para dar detalles del fallo
        }
        return conectar;
      
    }
}
