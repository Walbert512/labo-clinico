


import conexionSQL.conexionSQL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class historial extends javax.swing.JFrame {
    conexionSQL cc=new conexionSQL();
    Connection con=cc.conexion();
    

   
    public historial() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(getBackground());
        mostrarDatos();
       
    }
public void mostrarDatos(){
  ///Se crea una matriz que se usara para asignarle columnas a la tabla 
String[] historia={"Id Paciente","Id Historial","Nombre","Fecha","Tipo de examen"};
String[] registros=new String[6];
DefaultTableModel modelo=new DefaultTableModel(null,historia);
String SQL="SELECT * FROM historial JOIN registro USING(NRegistro)"; // Este es la instruccion que se envia a la base de datos


/////////////////////////////////////////////////////////////


try {
    //Abrimos connexion y enviamos la consulta SQL
Statement st=con.createStatement();
ResultSet rs=st.executeQuery(SQL);



while(rs.next()){
    
    // Alamacenamos las respuestas de la base de datos dentro de las columnas de la tabla
    registros[0]=rs.getString("NRegistro");
    registros[1]=rs.getString("idHistorial");
    registros[2]=rs.getString("Nombre");
    registros[3]=rs.getString("date");
    registros[4]=rs.getString("tipoExamen");
    
     modelo.addRow(registros);

   
    
}
    tabla.setModel(modelo);
} catch(Exception e){
    //En caso de error 
    JOptionPane.showMessageDialog(null,"Error al consultar datos " + e.getMessage());
}}



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrupo = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnNombre = new javax.swing.JRadioButton();
        btnID = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jCalendar1 = new com.toedter.calendar.JDateChooser();
        btnLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Historial");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(51, 153, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel1.setText("Historial");

        jLabel2.setText("Busqueda por nombre, ID o fecha:");

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btngrupo.add(btnNombre);
        btnNombre.setText("Nombre");

        btngrupo.add(btnID);
        btnID.setText("ID");
        btnID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIDActionPerformed(evt);
            }
        });

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tabla);

        btnLimpiar.setText("General");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(55, 55, 55))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnID)
                                .addComponent(btnNombre))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCalendar1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnLimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(247, 247, 247)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnID)
                        .addGap(5, 5, 5)
                        .addComponent(btnNombre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jCalendar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnBuscar)
                        .addGap(70, 70, 70)
                        .addComponent(btnLimpiar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void  validarBoton(){
        //Este if es si se selecciona el Nombre en el radio buton 
    if (btnNombre.isSelected()){
    
        String y = (txtBuscar.getText());
String[] historia={"Id Paciente","Id Historial","Nombre","Fecha","Tipo de examen"};
        String[] registros=new String[6];
        DefaultTableModel modelo=new DefaultTableModel(null,historia);
        String SQL="SELECT * FROM historial JOIN registro USING(NRegistro) where Nombre = '"+y+"'";

try {
Statement st=con.createStatement();
ResultSet rs=st.executeQuery(SQL);

while(rs.next()){
    registros[0]=rs.getString("NRegistro");
    registros[1]=rs.getString("idHistorial");
    registros[2]=rs.getString("Nombre");
    registros[3]=rs.getString("date");
    registros[4]=rs.getString("tipoExamen");
    
     modelo.addRow(registros);

   
    
}
    tabla.setModel(modelo);
  
} catch(Exception e){
    JOptionPane.showMessageDialog(null,"Error al consultar datos " + e.getMessage());
}
        
        
      //Este else if es si se selecciona el ID en el radio buton    
    }else if (btnID.isSelected()){
    
        String x = (txtBuscar.getText());
String[] historia={"Id Paciente","Id Historial","Nombre","Fecha","Tipo de examen"};
        String[] registros=new String[6];
        DefaultTableModel modelo=new DefaultTableModel(null,historia);
        String SQL="SELECT * FROM historial JOIN registro USING(NRegistro) where NRegistro = " + x;

try {
Statement st=con.createStatement();
ResultSet rs=st.executeQuery(SQL);

while(rs.next()){
    registros[0]=rs.getString("NRegistro");
    registros[1]=rs.getString("idHistorial");
    registros[2]=rs.getString("Nombre");
    registros[3]=rs.getString("date");
    registros[4]=rs.getString("tipoExamen");
    
     modelo.addRow(registros);

   
    
}
    tabla.setModel(modelo);
  
} catch(SQLException e){
    JOptionPane.showMessageDialog(null,"Error al consultar datos " + e.getMessage());
}
    }
    else {
     java.util.Date date =  jCalendar1.getDate();    
        
        long d = date.getTime();
        java.sql.Date fecha= new java.sql.Date(d);
       
    
               
String[] historia={"Id Paciente","Id Historial","Nombre","Fecha","Tipo de examen"};
        String[] registros=new String[6];
        DefaultTableModel modelo=new DefaultTableModel(null,historia);
        String SQL="SELECT * FROM historial JOIN pacientes USING(idPacientes) where date = " + "'" + fecha + "'";

try {
Statement st=con.createStatement();
ResultSet rs=st.executeQuery(SQL);

while(rs.next()){
    registros[0]=rs.getString("idPacientes");
    registros[1]=rs.getString("idHistorial");
    registros[2]=rs.getString("namePaciente");
    registros[3]=rs.getString("date");
    registros[4]=rs.getString("tipoExamen");
    
     modelo.addRow(registros);

   
    
}
    tabla.setModel(modelo);
  
} catch(SQLException e){
    JOptionPane.showMessageDialog(null,"Error al consultar datos " + e.getMessage());
}
       
    }
    
    }
    
    
    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
    validarBoton();

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnIDActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        mostrarDatos();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(historial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(historial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(historial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(historial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new historial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JRadioButton btnID;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JRadioButton btnNombre;
    private javax.swing.ButtonGroup btngrupo;
    private com.toedter.calendar.JDateChooser jCalendar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    private Object getContentPne() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void getText() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
